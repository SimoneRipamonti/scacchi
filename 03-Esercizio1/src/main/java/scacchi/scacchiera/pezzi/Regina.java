package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Coordinata;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Regina extends Pezzo {
	public Regina(Casella casella, Color color) {
		super(casella, color);
	}

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella destinazione) {

		// calcola la distanza della cella rispetto all�asse delle x e delle
		// y
		int deltaX = destinazione.getRow() - this.getCasella().getRow();
		int deltaY = destinazione.getCol() - this.getCasella().getCol();
		// se il valore assoluto delle distanze \�e diverso ritorna false
		if (Math.abs(deltaX) == Math.abs(deltaY)) {
			// controlla tutte le caselle sulla diagonale, se in una di
			// queste c�e� un pezzo ritorna false
			for (int i = 1; i < Math.abs(deltaX); i++) {
				if (!scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow()
								+ Integer.signum(deltaX) * i, this.getCasella()
								.getCol() + Integer.signum(deltaY) * i))
						.isEmpty()) {
					return false;
				}
			}
			return true;
		}
		if ((deltaX == 0) || (deltaY == 0)) {
			if (deltaY == 0) {
				// controlla tutte le sull�asse X, se in una di
				// queste c�e� un pezzo ritorna false
				for (int i = 1; i < Math.abs(deltaX); i++) {
					if (!scacchiera.getCasella(
							new Coordinata(this.getCasella().getRow() + i, this
									.getCasella().getCol())).isEmpty()) {
						return false;
					}
				}
			}
			if (deltaX == 0) {
				for (int i = 1; i < Math.abs(deltaY); i++) {
					if (!scacchiera.getCasella(
							new Coordinata(this.getCasella().getRow(), this
									.getCasella().getCol() + i)).isEmpty()) {
						return false;
					}
				}
			}
			return true;
		}
		// ritorna true
		return false;

	}

	@Override
	public String toString() {
		return "D [" + this.getColor() + "]";
	}
}
