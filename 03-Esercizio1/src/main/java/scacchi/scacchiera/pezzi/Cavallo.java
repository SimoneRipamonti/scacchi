package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Cavallo extends Pezzo {
	public Cavallo(Casella casella, Color color) {
		super(casella, color);
	}

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella destinazione) {

		// calcola la distanza della cella rispetto all�asse delle x e delle
		// y
		int deltaX = destinazione.getRow() - this.getCasella().getRow();
		int deltaY = destinazione.getCol() - this.getCasella().getCol();
		if ((Math.abs(deltaX) == 2 && Math.abs(deltaY) == 1)
				|| (Math.abs(deltaY) == 2 && Math.abs(deltaX) == 1)) {
			return true;
		}
		return false;

	}

	@Override
	public String toString() {
		return "C [" + this.getColor() + "]";
	}
}