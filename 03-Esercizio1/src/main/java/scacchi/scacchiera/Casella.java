package scacchi.scacchiera;

// casella estende coordinata
public class Casella extends Coordinata {
	private Pezzo pezzo;

	// che cosa succede se x<0 e se y<0? lo vedremo pi\�u avanti nel corso
	public Casella(int x, int y) {
		// chiama il costruttore di coordinata
		super(x, y);
		setPezzo(null);
	}

	public Pezzo getPezzo() {
		return pezzo;
	}

	// che cosa succede se il pezzo \�e nullo? lo vedremo pi\�u avanti nel corso
	public void setPezzo(Pezzo pezzo) {
		this.pezzo = pezzo;
	}

	public boolean isEmpty() {
		if (this.pezzo == null) {
			return true;
		}
		return false;
	}

	public Pezzo unsetPezzo() {
		Pezzo pieceToBeReturned = this.pezzo;
		this.pezzo = null;
		return pieceToBeReturned;
	}

	@Override
	public String toString() {
		if (this.getPezzo() != null) {
			return this.getPezzo().toString();
		} else {
			return " ";
		}
	}
}