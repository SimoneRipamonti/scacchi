package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Coordinata;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Torre extends Pezzo {
	
	private boolean fattaPrimaMossa;
	
	public Torre(Casella casella, Color color) {
		super(casella, color);
		this.fattaPrimaMossa = false;
	}

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella destinazione) {

		// se la riga o la colonna non e� costante ritorno false
		int deltaX = destinazione.getRow() - this.getCasella().getRow();
		int deltaY = destinazione.getCol() - this.getCasella().getCol();
		if (!((deltaX == 0) || (deltaY == 0))) {
			return false;
		}
		if (deltaY == 0) {
			// controlla tutte le caselle sull�asse X, se in una di queste
			// c�e� un pezzo ritorna false
			for (int i = 1; i < Math.abs(deltaX); i++) {
				if (!scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow() + i*Integer.signum(deltaX), this
								.getCasella().getCol())).isEmpty()) {
					return false;
				}
			}
		}
		if (deltaX == 0) {
			// controlla tutte le caselle sull�asse Y, se in una di queste
			// c�e� un pezzo ritorna false
			for (int i = 1; i < Math.abs(deltaY); i++) {
				if (!scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow(), this
								.getCasella().getCol() + i*Integer.signum(deltaY))).isEmpty()) {
					return false;
				}
			}
		}
		fattaPrimaMossa=true;
		return true;

	}

	@Override
	public String toString() {
		return "T [" + this.getColor() + "]";
	}

	public boolean isFattaPrimaMossa() {
		return fattaPrimaMossa;
	}

	public void setFattaPrimaMossa(boolean fattaPrimaMossa) {
		this.fattaPrimaMossa = fattaPrimaMossa;
	}
}
