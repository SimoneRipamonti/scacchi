package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Coordinata;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Alfiere extends Pezzo {
	public Alfiere(Casella casella, Color color) {
		super(casella, color);
	}

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella destinazione) {

		// calcola la distanza della cella rispetto all�asse delle x e delle
		// y
		int deltaX = destinazione.getRow() - this.getCasella().getRow();
		int deltaY = destinazione.getCol() - this.getCasella().getCol();
		// se il valore assoluto delle distanze \�e diverso ritorna false
		if (Math.abs(deltaX) != Math.abs(deltaY)) {
			return false;
		}
		// controlla tutte le caselle sulla diagonale, se in una di queste
		// c�e� un pezzo ritorna false
		for (int i = 1; i < Math.abs(deltaX); i++) {
			if (!scacchiera.getCasella(
					new Coordinata(this.getCasella().getRow()
							+ Integer.signum(deltaX) * i, this.getCasella()
							.getCol() + Integer.signum(deltaY) * i)).isEmpty()) {
				return false;
			}
		}
		// ritorna true
		return true;

	}

	@Override
	public String toString() {
		return "A [" + this.getColor() + "]";
	}
}