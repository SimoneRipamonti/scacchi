package scacchi.scacchiera;


public abstract class Pezzo {
	private final Color color;
	private Casella casella;

	public Pezzo(Casella casella, Color color) {
		this.setCasella(casella);
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public Casella getCasella() {
		return casella;
	}

	public void setCasella(Casella casella) {
		this.casella = casella;
	}
	
	public boolean controlloMossa(Scacchiera scacchiera, Casella destinazione){
		// esiste la destinazione?
		if (destinazione==null)
			return false;
		
		// c'� un pezzo dello stesso colore?
		if (!destinazione.isEmpty()
				&& destinazione.getPezzo().getColor() == this.getColor()) {
			return false;
		}
		
		// controllo se la mossa � valida
		boolean mossaValida = this.mossaValida(scacchiera, destinazione);
		if (mossaValida){
			scacchiera.incrementaTurno();
			return true;
		} else return false;
	}
	
	protected abstract boolean mossaValida(Scacchiera scacchiera,
			Casella destinazione);
}
