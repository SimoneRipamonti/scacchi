package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Coordinata;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Pedone extends Pezzo {
	private boolean fattaPrimaMossa;
	private int mossaSpecialeAlTurno;

	public Pedone(Casella casella, Color color) {
		super(casella, color);
		fattaPrimaMossa = false;
		mossaSpecialeAlTurno = -2;
	}

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella destinazione) {

		int deltaRow = destinazione.getRow() - this.getCasella().getRow();
		int deltaCol = destinazione.getCol() - this.getCasella().getCol();

		// un pedono non pu� muoversi all'indietro
		if (this.getColor().equals(Color.BLACK) && deltaRow > 0) {
			return false;
		}
		if (this.getColor().equals(Color.WHITE) && deltaRow < 0) {
			return false;
		}

		/*
		 * Prima mossa pedone: controllo se � la prima mossa del pedone;
		 * controllo se lo spostamento � di due righe; controllo che entrambe le
		 * caselle attraversate siano vuote; pongo il flag su fattaPrimaMossa e
		 * memorizzo in quale turno � stata fatta la mossa speciale; convalido
		 * la mossa;
		 */

		// implementazione della mossa iniziale
		if (this.fattaPrimaMossa == false) {
			// mi sto spostando di due righe, sulla stessa colonna, in una
			// casella vuota
			if (Math.abs(deltaRow) == 2
					&& Math.abs(deltaCol) == 0
					&& scacchiera.getCasella(
							new Coordinata(this.getCasella().getRow() + 2
									* Integer.signum(deltaRow), this
									.getCasella().getCol())).isEmpty()) {
				// controllo che la casella in mezzo sia libera
				if (scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow() + 1
								* Integer.signum(deltaRow), this.getCasella()
								.getCol())).isEmpty()) {
					this.fattaPrimaMossa = true;
					this.mossaSpecialeAlTurno = scacchiera.getTurno();
					return true;
				}
			}
		}

		if (Math.abs(deltaCol) > 1 || Math.abs(deltaRow) > 1) {
			return false;
		}

		/*
		 * En passant: mi sto muovendo in diagonale di una casella; la casella �
		 * vuota; il pedone che mi affianca ha appena mosso di 2; allora posso
		 * fare la presa enpassant;
		 */

		if (Math.abs(deltaCol) == 1
				&& Math.abs(deltaRow) == 1
				&& scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow() + deltaRow,
								this.getCasella().getCol() + deltaCol))
						.isEmpty()) {
			// presa en passant
			Casella casellaPedoneAvversario = scacchiera
					.getCasella(new Coordinata(this.getCasella().getRow(), this
							.getCasella().getCol() + deltaCol));
			if (casellaPedoneAvversario.getPezzo() instanceof Pedone) {
				Pedone pedoneAvversario = (Pedone) casellaPedoneAvversario
						.getPezzo();
				// il pedone avversario ha appena mosso in avanti di 2
				if (pedoneAvversario.getMossaSpecialeAlTurno() == scacchiera
						.getTurno() - 1) {
					// en passant valido
					casellaPedoneAvversario.unsetPezzo();
					return true;
				}
			} else
				return false;

		}

		if (Math.abs(deltaRow) == 1
				&& Math.abs(deltaCol) == 0
				&& !scacchiera.getCasella(
						new Coordinata(this.getCasella().getRow() + deltaRow,
								this.getCasella().getCol() + deltaCol))
						.isEmpty()) {
			return false;
		}
		this.fattaPrimaMossa = true;
		return true;

	}

	@Override
	public String toString() {
		return "P [" + this.getColor() + "]";
	}

	public int getMossaSpecialeAlTurno() {
		return mossaSpecialeAlTurno;
	}
}
