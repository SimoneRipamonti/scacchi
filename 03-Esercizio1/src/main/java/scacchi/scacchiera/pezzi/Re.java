package scacchi.scacchiera.pezzi;

import scacchi.scacchiera.Casella;
import scacchi.scacchiera.Color;
import scacchi.scacchiera.Coordinata;
import scacchi.scacchiera.Pezzo;
import scacchi.scacchiera.Scacchiera;

public class Re extends Pezzo {

	private boolean fattaPrimaMossa;

	public Re(Casella casella, Color color) {
		super(casella, color);
		this.fattaPrimaMossa = false;

	}

	// NOTA BENE: Coordinata(row, col)

	@Override
	public boolean mossaValida(Scacchiera scacchiera, Casella casellaFinale) {

		// arrocco Bianco Lungo

		if (this.getColor() == Color.WHITE && fattaPrimaMossa == false) {
			// il re si sta muovendo di due in orizzontale
			if (this.getCasella().getCol() - casellaFinale.getCol() == -2
					&& this.getCasella().getRow() == casellaFinale.getRow()
					&& casellaFinale.getRow() == 0) {
				// c'� una torre nell'angolo...
				Casella sorgenteTorre = scacchiera.getCasella(new Coordinata(0,
						7));
				if (sorgenteTorre.getPezzo() instanceof Torre) {
					// ... e che la torre non ha ancora mosso
					Torre mTorre = (Torre) sorgenteTorre.getPezzo();
					if (!mTorre.isFattaPrimaMossa()) {
						// controllo che tutte le caselle siano libere tra torre
						// e re
						int colTorre, colRe;
						colTorre = mTorre.getCasella().getCol();
						colRe = this.getCasella().getCol();
						for (int i = colTorre + 1; i < colRe; i++) {
							if (!scacchiera.getCasella(new Coordinata(0, i))
									.isEmpty()) {
								return false;
							}
						}
						// mossa arrocco � legale
						// sposto la torre
						Casella destinazioneTorre = scacchiera
								.getCasella(new Coordinata(0, 4));
						sorgenteTorre.unsetPezzo();
						destinazioneTorre.setPezzo(mTorre);
						mTorre.setCasella(destinazioneTorre);
						mTorre.setFattaPrimaMossa(true);
						// autorizzo la mossa del re
						this.fattaPrimaMossa = true;
						return true;

					}
				}
			}
		}

		// arrocco Bianco Corto

		if (this.getColor() == Color.WHITE && fattaPrimaMossa == false) {
			// il re si sta muovendo di due in orizzontale
			if (this.getCasella().getCol() - casellaFinale.getCol() == 2
					&& this.getCasella().getRow() == casellaFinale.getRow()
					&& casellaFinale.getRow() == 0) {
				// c'� una torre nell'angolo...
				Casella sorgenteTorre = scacchiera.getCasella(new Coordinata(0,
						0));
				if (sorgenteTorre.getPezzo() instanceof Torre) {
					// ... e che la torre non ha ancora mosso
					Torre mTorre = (Torre) sorgenteTorre.getPezzo();
					if (!mTorre.isFattaPrimaMossa()) {
						// controllo che tutte le caselle siano libere tra torre
						// e re
						int colTorre, colRe;
						colTorre = mTorre.getCasella().getCol();
						colRe = this.getCasella().getCol();
						for (int i = colRe + 1; i < colTorre; i++) {
							if (!scacchiera.getCasella(new Coordinata(0, i))
									.isEmpty()) {
								return false;
							}
						}
						// mossa arrocco � legale
						// sposto la torre
						Casella destinazioneTorre = scacchiera
								.getCasella(new Coordinata(0, 2));
						sorgenteTorre.unsetPezzo();
						destinazioneTorre.setPezzo(mTorre);
						mTorre.setCasella(destinazioneTorre);
						mTorre.setFattaPrimaMossa(true);
						// autorizzo la mossa del re
						this.fattaPrimaMossa = true;
						return true;

					}
				}

			}
		}

		// arrocco Nero Corto

		if (this.getColor() == Color.BLACK && fattaPrimaMossa == false) {
			// il re si sta muovendo di due in orizzontale
			if (this.getCasella().getCol() - casellaFinale.getCol() == 2
					&& this.getCasella().getRow() == casellaFinale.getRow()
					&& casellaFinale.getRow() == 7) {
				// c'� una torre nell'angolo...
				Casella sorgenteTorre = scacchiera.getCasella(new Coordinata(7,
						0));
				if (sorgenteTorre.getPezzo() instanceof Torre) {
					// ... e che la torre non ha ancora mosso
					Torre mTorre = (Torre) sorgenteTorre.getPezzo();
					if (!mTorre.isFattaPrimaMossa()) {
						// controllo che tutte le caselle siano libere tra torre
						// e re
						int colTorre, colRe;
						colTorre = mTorre.getCasella().getCol();
						colRe = this.getCasella().getCol();
						for (int i = colRe + 1; i < colTorre; i++) {
							if (!scacchiera.getCasella(new Coordinata(7, i))
									.isEmpty()) {
								return false;
							}
						}
						// mossa arrocco � legale
						// sposto la torre
						Casella destinazioneTorre = scacchiera
								.getCasella(new Coordinata(7, 2));
						sorgenteTorre.unsetPezzo();
						destinazioneTorre.setPezzo(mTorre);
						mTorre.setCasella(destinazioneTorre);
						// autorizzo la mossa del re
						mTorre.setFattaPrimaMossa(true);
						this.fattaPrimaMossa = true;
						return true;

					}
				}
			}

		}

		// arrocco Nero Lungo

		if (this.getColor() == Color.BLACK && fattaPrimaMossa == false) {
			// il re si sta muovendo di due in orizzontale
			if (this.getCasella().getCol() - casellaFinale.getCol() == -2
					&& this.getCasella().getRow() == casellaFinale.getRow()
					&& casellaFinale.getRow() == 7) {
				// c'� una torre nell'angolo...
				Casella sorgenteTorre = scacchiera.getCasella(new Coordinata(7,
						7));
				if (sorgenteTorre.getPezzo() instanceof Torre) {
					// ... e che la torre non ha ancora mosso
					Torre mTorre = (Torre) sorgenteTorre.getPezzo();
					if (!mTorre.isFattaPrimaMossa()) {
						// controllo che tutte le caselle siano libere tra torre
						// e re
						int colTorre, colRe;
						colTorre = mTorre.getCasella().getCol();
						colRe = this.getCasella().getCol();
						for (int i = colTorre + 1; i < colRe; i++) {
							if (!scacchiera.getCasella(new Coordinata(7, i))
									.isEmpty()) {
								return false;
							}
						}
						// mossa arrocco � legale
						// sposto la torre
						Casella destinazioneTorre = scacchiera
								.getCasella(new Coordinata(7, 4));
						sorgenteTorre.unsetPezzo();
						destinazioneTorre.setPezzo(mTorre);
						mTorre.setCasella(destinazioneTorre);
						// autorizzo la mossa del re
						mTorre.setFattaPrimaMossa(true);
						this.fattaPrimaMossa = true;
						return true;

					}
				}
			}
		}

		// ritorna true se la distanza tra la cella final e iniziale \�e di
		// una cella in orizzontale e in verticale
		if (Math.abs(this.getCasella().getRow() - casellaFinale.getRow()) <= 1
				&& Math.abs(this.getCasella().getCol() - casellaFinale.getCol()) <= 1) {
			fattaPrimaMossa = true;
			return true;
		}
		return false;

	}

	// ridefinisce il metodo toString ereditato dalla classe object. Se il
	// metodo toString viene chiamato
	// su un istanza di Re viene chiamato questo metodo invece di quello di
	// object
	@Override
	public String toString() {
		return "R [" + this.getColor() + "]";
	}
}